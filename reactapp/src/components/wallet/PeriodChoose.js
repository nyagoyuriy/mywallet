import React, { useState } from 'react';
import '../../style/PeriodChoose.css';
import DateRange from './DateRange';

const PeriodChoose = ({
          selectedPeriod,
          handlePeriodChoose,
          setStartDate,
          startDate,
          setEndDate,
          endDate
}) => {
    const [isModalOpen, setModalOpen] = useState(false);
    const [period, setPeriod] = useState('')


    const periods = [
        'Сегодня',
        'Текущая неделя',
        'Текущий месяц',
        'Предыдущий месяц',
        'Текущий год',
        'Выбрать период',
    ];

    const handleButtonClick = (period) => {
        if (period === 'Выбрать период') {
            setModalOpen(true);
            setPeriod(period)
        } else {
            handlePeriodChoose(period);
        }
    };

    return (
        <div className="period-choose">
            {periods.map((period) => (
                <button
                    key={period}
                    className={`period-button ${selectedPeriod === period ? 'active' : ''}`}
                    onMouseDown={() => handleButtonClick(period)}
                >
                    {period}
                </button>
            ))}
            {isModalOpen && (
                <DateRange
                    closeModal={() => setModalOpen(false)}
                    handlePeriodChoose={handlePeriodChoose}
                    setStartDate={setStartDate}
                    startDate={startDate}
                    setEndDate={setEndDate}
                    endDate={endDate}
                    period={period}
                />
            )}
        </div>
    );
};

export default PeriodChoose;

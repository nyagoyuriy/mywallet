import "../../style/DateRange.css"
import Modal from "../homepage/Modal";
import {useState} from "react";

const DateRange = ({
           closeModal,
           handlePeriodChoose,
           setStartDate,
           startDate,
           setEndDate,
           endDate,
           period
}) => {
    const [error, setError] = useState('');

    const handleSave = () => {
         if (!startDate || !endDate) {
            setError('Пожалуйста, введите обе даты.');
            return;
        }
        const formattedStartDate = startDate ? startDate + ' 00:00:00' : null;
        const formattedEndDate = endDate ? endDate + ' 23:59:59' : null;
        handlePeriodChoose(period);
        setStartDate(formattedStartDate)
        setEndDate(formattedEndDate)
        closeModal();
    };

    return (
        <Modal closeModal={closeModal}>
            <div className="date-range-modal">
                <h2>Выберите период</h2>
                {error && <div className="error-message">{error}</div>}
                <label>
                    Дата от:
                    <input
                        type="date"
                        value={startDate}
                        onChange={(e) => setStartDate(e.target.value)}

                    />
                </label>
                <label>
                    Дата до:
                    <input
                        type="date"
                        value={endDate}
                        onChange={(e) => setEndDate(e.target.value)}

                    />
                </label>
                <div className="modal-buttons">
                    <button onClick={handleSave}>Сохранить</button>
                    <button onClick={closeModal}>Отмена</button>
                </div>
            </div>
        </Modal>
    );
};

export default DateRange;

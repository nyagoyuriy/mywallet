import React, { useEffect, useState, useRef } from 'react';
import { Doughnut } from 'react-chartjs-2';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import '../../style/ExpenseChart.css';
import { chartColors } from '../../style/colors';

ChartJS.register(ArcElement, Tooltip, Legend);

const ExpenseChart = ({ data, sumValues, onChartClick, onOutsideChartClick  }) => {
    const [totalSum, setTotalSum] = useState(sumValues);
    const [centerTextLabel, setCenterTextLabel] = useState("Итого");
    const chartRef = useRef(null);

    const hasData = data.values.length > 0;

    useEffect(() => {
        setTotalSum(sumValues);
    }, [sumValues]);

    useEffect(() => {
        const handleClickOutside = (event) => {
            const chartContainer = chartRef.current;
            if (chartContainer && !chartContainer.contains(event.target)) {
                setCenterTextLabel("Итого");
                setTotalSum(sumValues);
                onOutsideChartClick()
            }
        };

        const handleClickInside = (event) => {
            const chartContainer = chartRef.current;
            if (chartContainer && chartContainer.contains(event.target)) {
                setCenterTextLabel("Итого");
                setTotalSum(sumValues);
                onOutsideChartClick();
            }
        };

        document.addEventListener('click', handleClickInside);
        document.addEventListener('click', handleClickOutside);
        return () => {
            document.removeEventListener('click', handleClickInside);
            document.removeEventListener('click', handleClickOutside);
        };
    }, [sumValues]);

    const chartData = {
        labels: hasData ? data.labels : ['No Data'],
        datasets: [
            {
                data: hasData ? data.values : [0.0001],
                backgroundColor: hasData ? chartColors : ['#d3d3d3'],
                hoverBackgroundColor: hasData ? chartColors : ['#d3d3d3'],
            },
        ],
    };

    const options = {
        cutout: '70%',
        responsive: true,
        plugins: {
            legend: {
                position: 'right',
                onClick: () => {},
            },
            tooltip: {
                enabled: true,
            },
            textCenter: {
                totalSum: hasData ? totalSum : 0,
                centerTextLabel: centerTextLabel,
            },
        },
        onClick: (event, elements) => {
            if (elements.length > 0) {
                const clickedElementIndex = elements[0].index;
                const label = chartData.labels[clickedElementIndex];
                const value = chartData.datasets[0].data[clickedElementIndex];
                setTotalSum(value);
                setCenterTextLabel(label);
                onChartClick(label);
            }
        },
    };

    const textCenter = {
        id: 'textCenter',
        beforeDraw(chart, args, pluginOptions) {
            const { ctx, chartArea } = chart;
            ctx.save();
            ctx.font = 'bold 20px Arial';
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            const centerX = (chartArea.left + chartArea.right) / 2;
            const centerY = (chartArea.top + chartArea.bottom) / 2;

            const topText = `${pluginOptions.centerTextLabel}:`;
            const bottomText = `${pluginOptions.totalSum}`;

            const lineHeight = 30;
            const topTextY = centerY - lineHeight / 2;
            const bottomTextY = centerY + lineHeight / 2;
            ctx.fillText(topText, centerX, topTextY);
            ctx.fillText(bottomText, centerX, bottomTextY);
            ctx.restore();
        },
    };

    return (
        <div className="chart-container" ref={chartRef}>
            <Doughnut data={chartData} options={options} plugins={[textCenter]} />
        </div>
    );
};

export default ExpenseChart;

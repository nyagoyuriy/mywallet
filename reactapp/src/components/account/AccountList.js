import React, { useState } from 'react';
import Modal from '../homepage/Modal';
import AccountForm from './AccountForm';
import useAxiosPrivate from '../../hooks/useAxiosPrivate';

const AccountList = ({ accounts, fetchAccounts, fetchTransactions }) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isConfirmModalOpen, setIsConfirmModalOpen] = useState(false);
    const [accountToDelete, setAccountToDelete] = useState(null);
    const [accountToEdit, setAccountToEdit] = useState(null);
    const [deleteTransactions, setDeleteTransactions] = useState(false);
    const [accountWithValueBalance, setAccountWithValueBalance] = useState(false)
    const axiosPrivate = useAxiosPrivate();

    const openModal = () => setIsModalOpen(true);
    const closeModal = () => {
        setIsModalOpen(false);
        setAccountToEdit(null); // Reset the account to edit when closing the modal
    };

    const handleAccountCreated = () => {
        fetchAccounts();
        closeModal();
    };

    const openConfirmModal = (account) => {
        setAccountToDelete(account);
        if (account.balance !== '0.00'){
            setDeleteTransactions(true)
            setAccountWithValueBalance(true)
        } else {
            setAccountWithValueBalance(false)
        }
        setIsConfirmModalOpen(true);
    };

    const closeConfirmModal = () => {
        setDeleteTransactions(false)
        setIsConfirmModalOpen(false);

    }

    const deleteAccount = async () => {
        try {
            await axiosPrivate.delete(`accounts/delete/${accountToDelete.id}/`,
                {data: {deleteTransactions}}
            );
            fetchAccounts();
            fetchTransactions();
            setDeleteTransactions(false)
            closeConfirmModal();
        } catch (error) {
            console.error('Failed to delete account:', error);
        }
    };

    const openEditModal = (account) => {
        setAccountToEdit(account);
        openModal();
    };
    const sumValues = accounts.reduce((sum, account) => sum + +parseFloat(account.balance), 0);
    const totalSum = parseFloat(sumValues).toFixed(2);

    return (
        <div id="accounts" className='container-with-padding'>
            <br/>
            <div className='account-list'>
                {accounts.map((account) => (
                    account.visible && (
                        <div key={account.id} className='account-card'>
                            <button className='edit-account-button' onClick={() => openEditModal(account)}>✏️</button>
                            <strong>{account.name}</strong><br />
                            {account.balance}{account.currency.symbol}
                            <button className='delete-account-button' onClick={() => openConfirmModal(account)}>×</button>
                        </div>
                    )
                ))}

                <div className='account-card add-account' onClick={openModal}>
                    <button className="add-button">+</button>
                </div>
            </div>
                <h2>Общая сумма: {totalSum}</h2>

            {isModalOpen && (
                <Modal closeModal={closeModal}>
                    <AccountForm
                        onAccountCreated={handleAccountCreated}
                        closeModal={closeModal}
                        accountToEdit={accountToEdit} // Pass the account to edit to the form
                    />
                </Modal>
            )}
            {isConfirmModalOpen && (
                <Modal closeModal={closeConfirmModal}>
                    <div>
                        <p>Вы уверены, что хотите удалить счет "{accountToDelete?.name}"?</p>
                        <div>
                                <input
                                    type="checkbox"
                                    id="deleteTransactions"
                                    name="deleteTransactions"
                                    checked={deleteTransactions}
                                    onChange={(e) =>
                                        setDeleteTransactions(e.target.checked)}
                                    disabled={accountWithValueBalance}
                                />
                            <label htmlFor="deleteTransactions">
                                Удалить все транзакции связанные с аккаунтом <br/>(В статистике не будут учтены)
                            </label>
                        </div>
                        {accountWithValueBalance && (
                            <div>
                                <br/>
                                <label>Нельзя сохранить транзакции для счета с ненулевым балансом</label>
                            </div>
                        )}
                        <button onClick={deleteAccount} className="confirm-delete-button">Удалить</button>
                        <button onClick={closeConfirmModal} className="cancel-delete-button">Отмена</button>
                    </div>
                </Modal>
            )}
        </div>
    );
};

export default AccountList;

import React, { useState, useEffect } from 'react';
import useAxiosPrivate from '../../hooks/useAxiosPrivate';

const AccountForm = ({ onAccountCreated, closeModal, accountToEdit }) => {
  const [name, setName] = useState('');
  const [balance, setBalance] = useState('');
  const axiosPrivate = useAxiosPrivate();

  useEffect(() => {
        if (accountToEdit) {
            setName(accountToEdit.name);
            setBalance(accountToEdit.balance);
        }
    }, [accountToEdit]);

  const handleNameChange = (event) => {
    setName(event.target.value);
  };

  const handleBalanceChange = (event) => {
    setBalance(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      if (accountToEdit) {
        await axiosPrivate.patch('accounts/update/' + accountToEdit.id + '/', {
          name,
          balance
        });
        onAccountCreated();
      } else {
        const response = await axiosPrivate.post('accounts/create', {
          name,
          balance: parseFloat(balance),
        });
        setBalance("")
        setName("")
        onAccountCreated(); // Call the handler to update the account list
      }
    } catch (error) {
      console.error('Failed to create account:', error);
    }
  };

  const handleCancel = () => {
    closeModal(); // Close the modal when cancel button is clicked
  };

  return (
    <div className='container'>
      <h2>{accountToEdit ? 'Edit Account' : 'Create Account'}</h2>
      <form onSubmit={handleSubmit}>
        <div className='mb-3'>
          <label htmlFor='name' className='form-label'>Name</label>
          <input
            type='text'
            className='form-control'
            id='name'
            value={name}
            onChange={handleNameChange}
            required
          />
        </div>
        <div className='mb-3'>
          <label htmlFor='balance' className='form-label'>Balance</label>
          <input
            type='number'
            className='form-control'
            id='balance'
            value={balance}
            onChange={handleBalanceChange}
            required
          />
        </div>
        <div className='mb-3'>
          <button type='submit' className='btn btn-success me-2'>{accountToEdit ? 'Update' : 'Create'}</button>
          <button type='button' className='btn btn-secondary' onClick={handleCancel}>Cancel</button>
        </div>
      </form>
    </div>
  );
};

export default AccountForm;

// App.js
import '../../style/App.css';
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import LoginForm from '../auth/LoginForm';
import RegisterForm from "../auth/RegisterForm";
import HomePage from "../homepage/HomePage";
import PrivateRoute from "../auth/PrivateRoute";
import {AuthContextProvider} from "../../store/auth-context";

const App = () => (
  <AuthContextProvider>
    <Router>
      <Routes>
        <Route path="/home" element={<PrivateRoute element={HomePage} />} />
        <Route path="/login" element={<LoginForm />} />
        <Route path="/register" element={<RegisterForm />} />
      </Routes>
    </Router>
  </AuthContextProvider>
);

export default App;

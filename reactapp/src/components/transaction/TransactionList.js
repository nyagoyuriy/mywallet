import React, { useEffect, useState } from "react";
import useAxiosPrivate from "../../hooks/useAxiosPrivate";
import TransactionForm from "./TransactionForm";
import Modal from "../homepage/Modal";
import ReactPaginate from "react-paginate";
import "../../style/TransactionList.css";  // Импорт стилей

const TransactionList = ({ accounts, transactions, categories, fetchTransactions, fetchAccounts }) => {
    const axiosPrivate = useAxiosPrivate();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [isConfirmModalOpen, setIsConfirmModalOpen] = useState(false);
    const [transactionToDelete, setTransactionToDelete] = useState(null);
    const [transactionToEdit, setTransactionToEdit] = useState(null);
    const [currentPage, setCurrentPage] = useState(0);
    const transactionsPerPage = 8;

    useEffect(() => {
        fetchTransactions();
    }, []);

    const openModal = () => {
        setTransactionToEdit(null);
        setIsModalOpen(true);
    };

    const openEditModal = (transaction) => {
        setTransactionToEdit(transaction);
        setIsModalOpen(true);
    };

    const closeModal = () => setIsModalOpen(false);

    const handleTransactionCreated = () => {
        fetchTransactions();
        fetchAccounts(); // Call fetchAccounts when a transaction is created
        closeModal();
    };

    const openConfirmModal = (transaction) => {
        setTransactionToDelete(transaction);
        setIsConfirmModalOpen(true);
    };

    const closeConfirmModal = () => setIsConfirmModalOpen(false);

    const deleteTransaction = async () => {
        try {
            await axiosPrivate.delete(`/transactions/delete/${transactionToDelete.id}/`);
            fetchTransactions();
            fetchAccounts(); // Call fetchAccounts to update the account balances
            closeConfirmModal();
        } catch (error) {
            console.error('Failed to delete transaction:', error);
        }
    };

    const formatDate = (dt) => {
        const date = new Date(dt);
        const day = date.getDate();

        const monthNames = [
            "января", "февраля", "марта", "апреля", "мая", "июня",
            "июля", "августа", "сентября", "октября", "ноября", "декабря"
        ];
        const month = monthNames[date.getMonth()];

        return `${day} ${month}`;
    };

    const handlePageClick = (data) => {
        setCurrentPage(data.selected);
    };

    const offset = currentPage * transactionsPerPage;
    const currentTransactions = transactions.slice(offset, offset + transactionsPerPage);
    const pageCount = Math.ceil(transactions.length / transactionsPerPage);

    return (
        <div className='container-with-padding'>
            <h2 className="center-text">Транзакции</h2>
            <div className="transaction-card add-transaction" onClick={openModal}>
                <button className="add-button">+</button>
            </div>
            {currentTransactions.map((transaction) => (
                <div key={transaction.id} className="transaction-card">
                    <button className="edit-transaction-button" onClick={() => openEditModal(transaction)}>
                        ✏️
                    </button>
                    <button className="delete-transaction-button" onClick={() => openConfirmModal(transaction)}>
                        ×
                    </button>
                    <div className="transaction-grid">
                        {transaction.type.name_eng === 'income' || transaction.type.name_eng === 'expense' ? (
                            <div className="transaction-subcategory">
                                {transaction.account_change.name + " -> "}{transaction.subcategory.name}
                            </div>
                        ) : (
                            transaction.type.name_eng === 'transfer' && (
                                <div className="transaction-subcategory">
                                    {transaction.account_change.name + " -> "}{transaction.account_transfer.name}
                                </div>
                            )
                        )}
                        <div className="transaction-comment">
                            {transaction.comment}
                        </div>
                        <div className={`transaction-amount ${transaction.type.name_eng}`}>
                            {transaction.amount} {transaction.currency.symbol}
                        </div>
                        <div className="transaction-date">
                            {formatDate(transaction.created_date)}
                        </div>
                    </div>
                </div>
            ))}
            <ReactPaginate
                previousLabel={'← Назад'}
                nextLabel={'Вперед →'}
                breakLabel={'...'}
                breakClassName={'break-me'}
                pageCount={pageCount}
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                onPageChange={handlePageClick}
                containerClassName={'pagination'}
                activeClassName={'active'}
            />
            {isModalOpen && (
                <Modal closeModal={closeModal}>
                    <TransactionForm
                        accounts={accounts}
                        categories={categories}
                        onTransactionCreated={handleTransactionCreated}
                        closeModal={closeModal}
                        transactionToEdit={transactionToEdit}
                    />
                </Modal>
            )}
            {isConfirmModalOpen && (
                <Modal closeModal={closeConfirmModal}>
                    <div>
                        <p>Вы уверены, что хотите удалить транзакцию "{transactionToDelete?.comment}"?</p>
                        <button onClick={deleteTransaction} className="confirm-delete-button">Да</button>
                        <button onClick={closeConfirmModal} className="cancel-delete-button">Нет</button>
                    </div>
                </Modal>
            )}
        </div>
    );
};

export default TransactionList;

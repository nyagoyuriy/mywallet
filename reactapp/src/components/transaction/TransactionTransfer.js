import React, {useEffect, useState} from 'react';

const TransactionTransfer = ({
    amount,
    setAmount,
    comment,
    setComment,
    selectedAccountChange,
    setSelectedAccountChange,
    selectedAccountTransfer,
    setSelectedAccountTransfer,
    accounts,
    transactionToEdit,
    selectedDate,
    setSelectedDate,
    selectedTime,
    setSelectedTime
}) => {
    const [filteredAccountsTransfer, setFilteredAccountsTransfer] = useState([]);
    const [filteredAccountsChange, setFilteredAccountsChange] = useState([]);
    useEffect(() => {
        setFilteredAccountsChange(
            accounts
                .filter((account) => account.id !== parseInt(selectedAccountTransfer))
                .map((account) => (
                    <option key={account.id} value={account.id}>
                        {account.name}
                    </option>
                ))
        );
    }, [accounts, selectedAccountTransfer]);

    useEffect(() => {
        setFilteredAccountsTransfer(
            accounts
                .filter((account) => account.id !== parseInt(selectedAccountChange))
                .map((account) => (
                    <option key={account.id} value={account.id}>
                        {account.name}
                    </option>
                ))
        );
    }, [accounts, selectedAccountChange]);


    return (
        <>
            <label>
                Сумма:
                <input
                    type="number"
                    value={amount}
                    onChange={(e) => setAmount(e.target.value)}
                    required
                />
            </label>
            <label htmlFor="comment">Комментарий</label>
            <textarea
                id="comment"
                className="form-input"
                value={comment}
                onChange={(e) => setComment(e.target.value)}
            />
            {transactionToEdit && (
                <div className="transaction-form-row">
                    <label className="transaction-form-label">Дата:
                        <input
                            type="date"
                            value={selectedDate}
                            onChange={(e) => setSelectedDate(e.target.value)} // Обработка изменения, если нужно
                            required
                        />
                    </label>
                    <label className="transaction-form-label">Время:
                        <input
                            type="time"
                            value={selectedTime}
                            onChange={(e) => setSelectedTime(e.target.value)} // Обработка изменения, если нужно
                            required
                        />
                    </label>
                </div>
            )}
            <div className="transaction-form-row">
                <label className="transaction-form-label">
                    Со счета:
                    <select
                        value={selectedAccountChange}
                        onChange={(e) =>
                            setSelectedAccountChange(e.target.value)}
                        required
                        className="transaction-form-input"
                    >
                        <option value="">Выберите счет</option>
                        {filteredAccountsChange}
                    </select>
                </label>
                <label className="transaction-form-label">
                    На счет:
                    <select
                        value={selectedAccountTransfer}
                        onChange={(e) => setSelectedAccountTransfer(e.target.value)}
                        required
                        className="transaction-form-input"
                    >
                        <option value="">Выберите счет</option>
                        {filteredAccountsTransfer}
                    </select>
                </label>
            </div>
        </>
    );
};

export default TransactionTransfer;

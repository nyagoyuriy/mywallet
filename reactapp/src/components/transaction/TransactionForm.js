import React, {useEffect, useState} from 'react';
import useAxiosPrivate from '../../hooks/useAxiosPrivate';
import '../../style/TransactionForm.css';
import TransactionTransfer from './TransactionTransfer';
import TransactionFinancial from './TransactionFinancial';
import transactionTransfer from "./TransactionTransfer";
import {set} from "react-hook-form";

const TransactionForm = ({ accounts, categories, onTransactionCreated, closeModal, transactionToEdit }) => {
    const [transactionType, setTransactionType] = useState(transactionToEdit ? transactionToEdit.type.name_eng : 'expense');
    const [amount, setAmount] = useState('');
    const [comment, setComment] = useState('');
    const [selectedCategory, setSelectedCategory] = useState('')
    const [selectedSubCategory, setSelectedSubCategory] = useState('');
    const [selectedAccountChange, setSelectedAccountChange] = useState('');
    const [selectedAccountTransfer, setSelectedAccountTransfer] = useState('');
    const [selectedDate, setSelectedDate] = useState('')
    const [selectedTime, setSelectedTime] = useState('')

    const axiosPrivate = useAxiosPrivate();

    useEffect(() => {
        if (transactionToEdit) {
            setAmount(transactionToEdit.amount);
            setComment(transactionToEdit.comment);
            const date = new Date(transactionToEdit.created_date);
            const isoDate = date.toISOString().split('T')[0]; // Получаем дату в формате "yyyy-mm-dd"
            const isoTime = date.toTimeString().slice(0, 5); // Получаем время в формате "hh:mm"
            setSelectedDate(isoDate);
            setSelectedTime(isoTime);
            setSelectedAccountChange(transactionToEdit.account_change.id);
            if (transactionType === 'transfer') {
                setSelectedAccountTransfer(transactionToEdit.account_transfer.id)
            } else {
                setSelectedSubCategory(transactionToEdit.subcategory.id)
            }

        }
    }, [transactionToEdit]);
    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            let data;
            if (transactionType === 'transfer') {
                data = {
                    type_str: transactionType,
                    amount: parseFloat(amount),
                    comment,
                    account_change_id: parseInt(selectedAccountChange),
                    account_transfer_id: parseInt(selectedAccountTransfer),
                    subcategory_id: null
                };
            } else {
                data = {
                    type_str: transactionType,
                    amount: parseFloat(amount),
                    comment,
                    account_change_id: parseInt(selectedAccountChange),
                    subcategory_id: parseInt(selectedSubCategory),
                    account_transfer_id: null
                };
            }
            if (transactionToEdit){
                data.new_date = selectedDate
                data.new_time = selectedTime
                await axiosPrivate.patch('transactions/update/' + transactionToEdit.id + "/", data);
                onTransactionCreated();
            } else {
                await axiosPrivate.post('transactions/create', data);
                onTransactionCreated();
            }
        } catch (error) {
            console.error('Failed to create transaction:', error);
        }
    };

    return (
        <div className="transaction-form">
            <div className="transaction-type-buttons">
                <button
                    className={transactionType === 'income' ? 'active' : ''}
                    onClick={() => setTransactionType('income')}
                >
                    Доход
                </button>
                <button
                    className={transactionType === 'expense' ? 'active' : ''}
                    onClick={() => setTransactionType('expense')}
                >
                    Расход
                </button>
                <button
                    className={transactionType === 'transfer' ? 'active' : ''}
                    onClick={() => setTransactionType('transfer')}
                >
                    Перевод
                </button>
            </div>
            <form onSubmit={handleSubmit}>
                {transactionType === 'income' || transactionType === 'expense' ? (
                    <TransactionFinancial
                        amount={amount}
                        setAmount={setAmount}
                        comment={comment}
                        setComment={setComment}
                        selectedAccountChange={selectedAccountChange}
                        setSelectedAccountChange={setSelectedAccountChange}
                        selectedSubcategory={selectedSubCategory}
                        setSelectedSubcategory={setSelectedSubCategory}
                        selectedCategory={selectedCategory}
                        setSelectedCategory={setSelectedCategory}
                        accounts={accounts}
                        categories={categories}
                        transactionToEdit={transactionToEdit}
                        selectedDate={selectedDate}
                        setSelectedDate={setSelectedDate}
                        selectedTime={selectedTime}
                        setSelectedTime={setSelectedTime}
                    />
                ) : (
                    <TransactionTransfer
                        amount={amount}
                        setAmount={setAmount}
                        comment={comment}
                        setComment={setComment}
                        selectedAccountChange={selectedAccountChange}
                        setSelectedAccountChange={setSelectedAccountChange}
                        selectedAccountTransfer={selectedAccountTransfer}
                        setSelectedAccountTransfer={setSelectedAccountTransfer}
                        accounts={accounts}
                        transactionToEdit={transactionToEdit}
                        selectedDate={selectedDate}
                        setSelectedDate={setSelectedDate}
                        selectedTime={selectedTime}
                        setSelectedTime={setSelectedTime}

                    />
                )}
                <button type="submit">{transactionToEdit ? "Update Transaction" : "Add Transaction"}</button>
                <button type="button" onClick={closeModal}>Cancel</button>
            </form>
        </div>
    );
};

export default TransactionForm;

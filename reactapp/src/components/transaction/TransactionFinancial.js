

import React, {useEffect, useState} from 'react';

const TransactionFinancial = ({
        amount,
        setAmount,
        comment,
        setComment,
        selectedAccountChange,
        setSelectedAccountChange,
        selectedSubcategory,
        setSelectedSubcategory,
        selectedCategory,
        setSelectedCategory,
        accounts,
        categories,
        transactionToEdit,
        selectedDate,
        setSelectedDate,
        selectedTime,
        setSelectedTime
}) => {
    const [selectedCategoryId, setSelectedCategoryId] = useState('')

     useEffect(() => {
        if (transactionToEdit) {
          setSelectedCategoryId(transactionToEdit.category.id);
        }
      }, [transactionToEdit]);

      useEffect(() => {
        if (selectedCategoryId) {
          const category = categories.find(cat => cat.id === parseInt(selectedCategoryId));
          setSelectedCategory(category);
        }
      }, [selectedCategoryId, categories]);

    const handleCategoryChange = (e) => {
        const categoryId = e.target.value;
        setSelectedCategoryId(categoryId);
        const category = categories.find(cat => cat.id === parseInt(categoryId));
        setSelectedCategory(category);
    };



    return (
        <>
            <label>
                Сумма:
                <input
                    type="number"
                    value={amount}
                    onChange={(e) => setAmount(e.target.value)}
                    required
                />
            </label>
            <label htmlFor="comment">Комментарий</label>
            <textarea
                id="comment"
                className="form-input"
                value={comment}
                onChange={(e) => setComment(e.target.value)}
                required
            />
            {transactionToEdit && (
                <div className="transaction-form-row">
                    <label className="transaction-form-label">Дата:
                        <input
                            type="date"
                            value={selectedDate}
                            onChange={(e) => setSelectedDate(e.target.value)} // Обработка изменения, если нужно
                            required
                        />
                    </label>
                    <label className="transaction-form-label">Время:
                        <input
                            type="time"
                            value={selectedTime}
                            onChange={(e) => setSelectedTime(e.target.value)} // Обработка изменения, если нужно
                            required
                        />
                    </label>
                </div>
            )}
            <div className="transaction-form-row">
                <label>
                    Cчет:
                    <select
                        value={selectedAccountChange}
                        onChange={(e) => setSelectedAccountChange(e.target.value)}
                        required
                    >
                        <option value="">Выберите счет</option>
                        {accounts.map((account) => (
                            <option key={account.id} value={account.id}>
                                {account.name}
                            </option>
                        ))}
                    </select>

                </label>
                <div className="transaction-form-column">
                    <label>
                        Категория:
                        <select
                            value={selectedCategoryId}
                            onChange={handleCategoryChange}
                            required
                        >
                            <option value="">Выберите категорию</option>
                            {categories.map((category) => (
                                <option key={category.id} value={category.id}>
                                    {category.name}
                                </option>
                            ))}
                        </select>
                    </label>
                    {selectedCategory && (
                        <label>
                        Подкатегория
                            <select
                                value={selectedSubcategory}
                                onChange={(e) => setSelectedSubcategory(e.target.value)}
                                required
                            >
                                <option value="">Выберите подкатегорию</option>
                                {selectedCategory.subcategories.map((subcategory) => (
                                    <option key={subcategory.id} value={subcategory.id}>
                                        {subcategory.name}
                                    </option>
                                ))}
                            </select>
                        </label>
                        )}
                    </div>

            </div>
        </>
    );
};

export default TransactionFinancial;


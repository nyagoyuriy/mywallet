import React, { useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import '../../style/VerticalMenu.css';
import useAxiosPrivate from "../../hooks/useAxiosPrivate";
import useAuth from "../../hooks/useAuth";

const VerticalMenu = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const navigate = useNavigate();
  const { setUser, setAccessToken, setCSRFToken } = useAuth();
  const axiosPrivate = useAxiosPrivate();

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  const handleLogout = async () => {
    try {
      await axiosPrivate.post('auth/logout', {}); // Очистка состояния аутентификации
      setUser(null);
      setAccessToken(null);
      setCSRFToken(null);
      navigate('/login');
    } catch (error) {
      console.error('Logout failed:', error);
    }
  };

  const scrollToSection = (sectionId) => {
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <div>
      <button className="menu-toggle" onClick={toggleMenu}>
        &#61;
      </button>
      <div className={`vertical-menu ${isMenuOpen ? 'open' : ''}`}>
        <NavLink
          to="/home"
          className={({ isActive }) => "menu-item" + (isActive ? " active" : "")}
        >
          Главная
        </NavLink>
        <div
          className="menu-item"
          onClick={() => scrollToSection('accounts')}
        >
          Счета
        </div>
        <div
          className="menu-item"
          onClick={() => scrollToSection('statistic')}
        >
          Статистика
        </div>
        <div
          className="menu-item"
          onClick={() => scrollToSection('transactions')}
        >
          Транзакции
        </div>
        <div className="menu-item" onClick={() => handleLogout()}>
          Выход
        </div>
      </div>
    </div>
  );
};

export default VerticalMenu;

import React, { useEffect, useState, useRef } from 'react';
import AccountList from '../account/AccountList';
import TransactionList from '../transaction/TransactionList';
import useAxiosPrivate from '../../hooks/useAxiosPrivate';
import ExpenseChart from "../wallet/ExpenseChart";
import VerticalMenu from "./VerticalMenu";
import PeriodChoose from "../wallet/PeriodChoose";

const HomePage = () => {
    const axiosPrivate = useAxiosPrivate();
    const [accounts, setAccounts] = useState([]);
    const [transactions, setTransactions] = useState([]);
    const [filteredTransactions, setFilteredTransactions] = useState([]);
    const [categories, setCategories] = useState([]);
    const [chartLabels, setChartLabels] = useState([]);
    const [chartValues, setChartValues] = useState([]);
    const [selectedPeriod, setSelectedPeriod] = useState('Текущий месяц');
    const [totalExpense, setTotalExpense] = useState(0);
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');


    const chart_data = {
        labels: chartLabels,
        values: chartValues
    };

    const fetchAccounts = async () => {
        try {
            const response = await axiosPrivate.get('accounts/get');
            setAccounts(response.data);
        } catch (error) {
            console.error('Failed to fetch accounts:', error);
        }
    };

    const fetchTransactions = async () => {
        try {
            const response = await axiosPrivate.get('transactions/get', {
                params: {
                    period: selectedPeriod,
                    start_date: startDate || null,
                    end_date: endDate || null
                }
            });
            setTransactions(response.data['transactions']);
            setChartLabels(response.data['chart_labels']);
            setChartValues(response.data['chart_values']);
        } catch (error) {
            console.error('Failed to fetch transactions:', error);
        }
    };

    const fetchCategories = async () => {
        try {
            const categoriesResponse = await axiosPrivate.get('categories/get');
            setCategories(categoriesResponse.data);
        } catch (error) {
            console.error('Failed to fetch categories:', error);
        }
    };

    const onChartClick = (categoryName) => {
        const filtered = transactions.filter(transaction =>
            transaction.category && transaction.category.name === categoryName
        );
        setFilteredTransactions(filtered);
    };

    const onOutsideChartClick = () => {
        setFilteredTransactions([]);
    };

    const handlePeriodChoose = (period) => {
        setSelectedPeriod(period);
    };

    useEffect(() => {
        fetchAccounts();
        fetchCategories();
    }, []);

    useEffect(() => {
        setFilteredTransactions([]);
        fetchTransactions();
    }, [selectedPeriod]);

    useEffect(() => {
        const sum = chartValues.reduce((acc, value) => acc + value, 0);
        setTotalExpense(sum.toFixed(2));
    }, [chartValues]);

    return (
        <div>
            <div className="main_content">
                <VerticalMenu />
            </div>
            <div className='container mt-4'>
                <AccountList accounts={accounts} fetchAccounts={fetchAccounts} fetchTransactions={fetchTransactions} />
            </div>
            <hr />
            <h2 id="statistic" className="center-text">Структура расходов</h2>
            <div className="horizontal-container">
                <PeriodChoose
                    selectedPeriod={selectedPeriod}
                    handlePeriodChoose={handlePeriodChoose}
                    startDate={startDate}
                    setStartDate={setStartDate}
                    endDate={endDate}
                    setEndDate={setEndDate}
                />
                <ExpenseChart
                    data={chart_data}
                    sumValues={totalExpense}
                    onChartClick={onChartClick}
                    onOutsideChartClick={onOutsideChartClick}
                />
            </div>
            <div id="transactions" className="transaction-list-container">
                <TransactionList
                    accounts={accounts}
                    transactions={filteredTransactions.length > 0 ? filteredTransactions : transactions}
                    categories={categories}
                    fetchTransactions={fetchTransactions}
                    fetchAccounts={fetchAccounts}
                />
            </div>
        </div>
    );
};

export default HomePage;

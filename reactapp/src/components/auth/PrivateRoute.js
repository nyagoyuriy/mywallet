import React, { useEffect, useState } from 'react';
import { Navigate } from 'react-router-dom';
import useAuth from "../../hooks/useAuth";
import useAxiosPrivate from "../../hooks/useAxiosPrivate";

const PrivateRoute = ({ element: Component, ...rest }) => {
    const { setUser } = useAuth()
    const [isAuthenticated, setIsAuthenticated] = useState(null);
    const axiosPrivate = useAxiosPrivate();


  useEffect(() => {
    const verifyUser = async () => {
            try {
                const { data } = await axiosPrivate.get('/auth/user');
                setUser(data);
                setIsAuthenticated(true);
            } catch (error) {
                console.log(error?.response);
                setIsAuthenticated(false);
            }
        };
        verifyUser()
  }, [axiosPrivate, setUser]);
    if (isAuthenticated === null) {
        return <div>Loading...</div>;
    }

  return isAuthenticated ? <Component {...rest} /> : <Navigate to="/login" />;
};

export default PrivateRoute;

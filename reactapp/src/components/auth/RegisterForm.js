import React from 'react';
import axios from 'axios';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

const RegisterForm = () => {
  const { register, handleSubmit, formState: { errors } } = useForm();
  const navigate = useNavigate();

  const onSubmit = async (data) => {
    try {
      const response = await axios.post('http://localhost:8000/auth/register', data);
      console.log('Registration successful:', response.data);
      // После успешной регистрации, вы можете автоматически переключиться на форму логина
      navigate("/login")
    } catch (error) {
      console.error('Registration failed:', error.response.data);
    }
  };

  return (
      <form onSubmit={handleSubmit(onSubmit)}>
          <div>
              <label htmlFor="username">Username</label>
              <input className="form-input"
                     id="username"
                     name="username"
                     type="text"
                     {...register('username', {required: 'Username is required'})}
              />
              {errors.username && <p>{errors.username.message}</p>}
          </div>

          <div>
              <label htmlFor="password">Password</label>
              <input className="form-input"
                     id="password"
                     name="password"
                     type="password"
                     {...register('password', {required: 'Password is required'})}
              />
              {errors.password && <p>{errors.password.message}</p>}
          </div>
          <div>
              <label htmlFor="password2">Password</label>
              <input className="form-input"
                     id="password2"
                     name="password2"
                     type="password"
                     {...register('password2', {required: 'Password is required'})}
              />
              {errors.password && <p>{errors.password.message}</p>}
          </div>

          <div>
              <label htmlFor="email">Email</label>
              <input className="form-input"
                     id="email"
                     name="email"
                     type="email"
                     {...register('email', {required: 'Email is required'})}
              />
              {errors.email && <p>{errors.email.message}</p>}
          </div>

          <button type="submit" className="form-button">Register</button>
          <p>Already have an account? <button type="button" className="form-button"
                                              onClick={() => navigate('/login')}>Login here</button></p>
      </form>
  );
};

export default RegisterForm;

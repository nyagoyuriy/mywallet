from django.urls import path
from apps.authentication.views import login_view, register_view, CookieTokenRefreshView, get_user_view, logout_view
from apps.category.views import get_categories_view
from apps.wallet.views import get_user_accounts_view, create_account_view, delete_account_view, get_transactions_view, \
    create_transaction_view, update_account_view, update_transaction_view, delete_transaction_view

urlpatterns = [
    path('auth/login', login_view, name='auth-login'),
    path('auth/logout', logout_view),
    path('auth/register', register_view, name='auth-register'),
    path('auth/refresh-token', CookieTokenRefreshView.as_view(), name='auth-refresh_token'),
    path('auth/user', get_user_view),

    path('accounts/get', get_user_accounts_view),
    path('accounts/create', create_account_view),
    path('accounts/delete/<int:id>/', delete_account_view),
    path('accounts/update/<int:id>/', update_account_view),

    path('transactions/get', get_transactions_view),
    path('transactions/create', create_transaction_view),
    path('transactions/update/<int:id>/', update_transaction_view),
    path('transactions/delete/<int:id>/', delete_transaction_view),

    path('categories/get', get_categories_view),

]
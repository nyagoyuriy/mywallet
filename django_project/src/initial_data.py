# -*- coding: utf-8 -*-
from django.db import migrations

from apps.category.models import Category, SubCategory


def add_initial_categories(apps, schema_editor):
    # Создаем категории
    categories = [
        {'name': 'Еда и напитки', 'subcategories': ['Продукты', 'Рестораны, кафе']},
        {'name': 'Жилье', 'subcategories': ['Аренда', 'Ипотека', 'Коммунальные услуги']},
        {'name': 'Доход', 'subcategories': ['Зарплата', 'Инвестиции']}
    ]

    for category_data in categories:
        category_name = category_data['name']
        subcategories = category_data['subcategories']

        # Создаем категорию
        category_obj, created = Category.objects.get_or_create(name=category_name)

        # Создаем подкатегории для данной категории
        for subcategory_name in subcategories:
            SubCategory.objects.get_or_create(category=category_obj, name=subcategory_name)


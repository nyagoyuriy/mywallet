from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=100, unique=True)

class SubCategory(models.Model):
    category = models.ForeignKey(Category, related_name='subcategories', on_delete=models.CASCADE)
    name = models.CharField(max_length=100, unique=True)




def add_initial_categories():
    # Создаем категории
    categories = [
        {'name': 'Еда и напитки', 'subcategories': ['Продукты', 'Рестораны, кафе']},
        {'name': 'Жилье', 'subcategories': ['Аренда', 'Ипотека', 'Коммунальные услуги']},
        {'name': 'Доход', 'subcategories': ['Зарплата', 'Инвестиции']}
    ]

    for category_data in categories:
        category_name = category_data['name']
        subcategories = category_data['subcategories']

        # Создаем категорию
        category_obj, created = Category.objects.get_or_create(name=category_name)

        # Создаем подкатегории для данной категории
        for subcategory_name in subcategories:
            SubCategory.objects.get_or_create(category=category_obj, name=subcategory_name)


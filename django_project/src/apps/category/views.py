from rest_framework import decorators as rest_decorators
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.category.models import SubCategory, Category
from apps.category.serializers import SubcategorySerializer, CategorySerializer


@rest_decorators.api_view(["GET"])
@rest_decorators.permission_classes([IsAuthenticated])
def get_categories_view(request):
    categories = Category.objects.all()
    serializer = CategorySerializer(categories, many=True)
    return Response(serializer.data)
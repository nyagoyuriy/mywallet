# Generated by Django 5.0.6 on 2024-06-29 06:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('currency', '0002_currency_created_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='currency',
            name='symbol',
            field=models.CharField(default='₽'),
        ),
    ]

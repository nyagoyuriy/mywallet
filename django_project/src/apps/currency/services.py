'''
def get_curracy() -> float:
    BASE_URL = "https://www.cbr.ru/scripts/XML_daily.asp"
    resp = requests.post(BASE_URL)
    root = ET.fromstring(resp.content)
    USD_elem = root.findall(".//*[@ID='R01235']")
    return float(USD_elem[0].find("Value").text.replace(",", "."))
'''
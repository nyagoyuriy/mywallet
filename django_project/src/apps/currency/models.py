from django.db import models


class Currency(models.Model):
    currency_id = models.CharField(unique=True)
    num_code = models.CharField(max_length=3, unique=True)
    char_code = models.CharField(max_length=3, unique=True)
    nominal = models.IntegerField()
    value = models.DecimalField(max_digits=10, decimal_places=2)
    name = models.CharField()
    symbol = models.CharField(default="")
    created_date = models.DateTimeField(auto_now_add=True)
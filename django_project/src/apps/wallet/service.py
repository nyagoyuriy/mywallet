from django.db import transaction
from django.db.models import Sum
from django.shortcuts import get_object_or_404
from django.utils import timezone
from datetime import timedelta, datetime

from apps.category.models import SubCategory
from apps.wallet.models import Transaction, TransactionType, Account


class AccountingEntry:
    def __init__(self, transaction: Transaction):
        self.transaction = transaction

    def account_income(self):
        self.transaction.account_change.balance += self.transaction.amount

    def account_expense(self):
        self.transaction.account_change.balance -= self.transaction.amount

    def account_transfer(self):
        self.transaction.account_change.balance -= self.transaction.amount
        self.transaction.account_transfer.balance += self.transaction.amount

    def account_transfer_inverse(self):
        self.transaction.account_change.balance += self.transaction.amount
        self.transaction.account_transfer.balance -= self.transaction.amount

    def create_entry(self):
        with transaction.atomic():
            if self.transaction.type.name_eng == "income":
                self.account_income()
            elif self.transaction.type.name_eng == "expense":
                self.account_expense()
            elif self.transaction.type.name_eng == "transfer":
                self.account_transfer()

            # Save changes to accounts
            self.transaction.account_change.save()
            if self.transaction.type.name_eng == "transfer":
                self.transaction.account_transfer.save()

    def delete_entry_accounts_update(self):
        if self.transaction.type.name_eng == "income":
            self.transaction.account_change.balance -= self.transaction.amount
        if self.transaction.type.name_eng == "expense":
            self.transaction.account_change.balance += self.transaction.amount
        if self.transaction.type.name_eng == "transfer":
            self.transaction.account_change.balance += self.transaction.amount
            self.transaction.account_transfer.balance -= self.transaction.amount
            self.transaction.account_transfer.save()
        self.transaction.account_change.save()


class UpdateAccountingEntry(AccountingEntry):
    def __init__(self,
                 instance: Transaction,
                 new_transaction_type: str,
                 subcategory_id: int,
                 account_change_id: int,
                 account_transfer_id: int,
                 new_amount: float):

        super().__init__(instance)
        self.new_transaction_type = new_transaction_type
        self.subcategory_id = subcategory_id
        self.account_change_id = account_change_id
        self.account_transfer_id = account_transfer_id
        self.amount = new_amount

    @staticmethod
    def update_transaction_account(user_id, account_id):
        with transaction.atomic():
            new_id = get_object_or_404(Account, wallet_user_id=user_id, name="Вне")
            Transaction.objects.filter(
                wallet_user=user_id,
                account_change_id=account_id
            ).update(account_change_id=new_id)

            Transaction.objects.filter(
                wallet_user=user_id,
                account_transfer_id=account_id
            ).update(account_transfer_id=new_id)


    def adjust_balance(self, direction: int):
            old_account_change = self.transaction.account_change
            account_change_delta = direction * self.transaction.amount

            if old_account_change.id == self.account_change_id:
                account_change_delta += -direction * self.amount
                old_account_change.balance += account_change_delta
            else:
                old_account_change.balance += account_change_delta
                new_change_account = Account.objects.get(id=self.account_change_id)
                self.transaction.account_change = new_change_account
                new_change_account.balance += -direction * self.amount
                new_change_account.save()
            old_account_change.save()

            self.transaction.amount = self.amount
            self.transaction.subcategory = SubCategory.objects.get(id=self.subcategory_id)

    def update_transfer(self):
        old_account_change = self.transaction.account_change
        old_account_transfer = self.transaction.account_transfer
        account_change_delta = self.transaction.amount
        account_transfer_delta = -self.transaction.amount

        if old_account_change.id == self.account_change_id:
            account_change_delta -= self.amount
            old_account_change.balance += account_change_delta
        else:
            old_account_change.balance += account_change_delta
            new_change_account = Account.objects.get(id=self.account_change_id)
            self.transaction.account_change = new_change_account
            new_change_account.balance -= self.amount
            new_change_account.save()
        old_account_change.save()

        if old_account_transfer.id == self.account_transfer_id:
            account_transfer_delta += self.amount
            old_account_transfer.balance += account_transfer_delta
        else:
            old_account_transfer.balance += account_transfer_delta
            new_account_transfer = Account.objects.get(id=self.account_transfer_id)
            self.transaction.account_transfer = new_account_transfer
            new_account_transfer.balance += self.amount
            new_account_transfer.save()
        old_account_transfer.save()
        self.transaction.amount = self.amount

    def update_same_type_transaction(self):
        if self.transaction.type.name_eng == "transfer":
            self.update_transfer()
        if self.transaction.type.name_eng == "income":
            self.adjust_balance(-1)
        if self.transaction.type.name_eng == "expense":
            self.adjust_balance(1)

    def income_to_expense(self, direction: int):
        old_account_change = self.transaction.account_change
        account_change_delta = direction * self.transaction.amount
        if old_account_change.id == self.account_change_id:
            account_change_delta += direction * self.amount
            old_account_change.balance += account_change_delta
        else:
            old_account_change.balance += account_change_delta
            new_account_change = Account.objects.get(id=self.account_change_id)
            self.transaction.account_change = new_account_change
            new_account_change.balance += direction * self.amount
            new_account_change.save()
        old_account_change.save()
        self.transaction.amount = self.amount
        self.transaction.subcategory = SubCategory.objects.get(id=self.subcategory_id)

    def income_expense_to_transfer(self, direction: int):
        old_account_change = self.transaction.account_change
        account_change_delta = direction * self.transaction.amount
        if old_account_change.id == self.account_change_id:
            account_change_delta -= self.amount
            old_account_change.balance += account_change_delta
        else:
            old_account_change.balance += account_change_delta
            new_account_change = Account.objects.get(id=self.account_change_id)
            self.transaction.account_change = new_account_change
            new_account_change.balance -= self.amount
            new_account_change.save()
        old_account_change.save()

        new_account_transfer = Account.objects.get(id=self.account_transfer_id)
        new_account_transfer.balance += self.amount
        new_account_transfer.save()

        self.transaction.account_transfer = new_account_transfer
        self.transaction.subcategory = None
        self.transaction.amount = self.amount

    def transfer_to_income_expense(self, direction: int):
        old_account_change = self.transaction.account_change
        account_change_delta = self.transaction.amount
        if old_account_change.id == self.account_change_id:
            account_change_delta += -direction * self.amount
            old_account_change.balance += account_change_delta
        else:
            old_account_change.balance += account_change_delta
            new_account_change = Account.objects.get(id=self.account_change_id)
            self.transaction.account_change = new_account_change
            new_account_change.balance += -direction * self.amount
            new_account_change.save()
        old_account_change.save()

        self.transaction.account_transfer.balance -= self.transaction.amount
        self.transaction.account_transfer.save()
        self.transaction.account_transfer = None

        self.transaction.subcategory = SubCategory.objects.get(id=self.subcategory_id)
        self.transaction.amount = self.amount

    def update_different_type_transaction(self):
        if self.transaction.type.name_eng == "income" and self.new_transaction_type == "expense":
            self.income_to_expense(direction=-1)
        if self.transaction.type.name_eng == "expense" and self.new_transaction_type == "income":
            self.income_to_expense(direction=1)
        if self.transaction.type.name_eng == "transfer" and self.new_transaction_type == "income":
            self.transfer_to_income_expense(-1)
        if self.transaction.type.name_eng == "transfer" and self.new_transaction_type == "expense":
            self.transfer_to_income_expense(1)
        if self.transaction.type.name_eng == "income" and self.new_transaction_type == "transfer":
            self.income_expense_to_transfer(direction=-1)
        if self.transaction.type.name_eng == "expense" and self.new_transaction_type == "transfer":
            self.income_expense_to_transfer(direction=1)
        self.transaction.type = TransactionType.objects.get(name_eng=self.new_transaction_type)

    def update_entry(self):
        if self.transaction.type.name_eng == self.new_transaction_type:
            self.update_same_type_transaction()
        else:
            self.update_different_type_transaction()

class TransactionsFilter:
    def __init__(self, transactions, period, start_date=None, end_date=None):
        self.transactions = transactions
        self.period = period
        self.start_date = start_date
        self.end_date = end_date

    def get_timeframe(self):
        now = timezone.now()
        if self.period == "Сегодня":
            start_date = now.replace(hour=0, minute=0, second=0, microsecond=0)
            end_date = now
        elif self.period == "Текущая неделя":
            start_date = now.replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=now.weekday())
            end_date = now
        elif self.period == "Текущий месяц":
            start_date = now.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
            end_date = now
        elif self.period == "Предыдущий месяц":
            first_day_prev_month = (now.replace(day=1, hour=0, minute=0, second=0, microsecond=0) - timedelta(days=1)).replace(day=1)
            start_date = first_day_prev_month
            end_date = now.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        elif self.period == "Текущий год":
            start_date = now.replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0)
            end_date = now
        else:
            start_datetime = datetime.strptime(self.start_date, "%Y-%m-%d %H:%M:%S")
            end_datetime = datetime.strptime(self.end_date, "%Y-%m-%d %H:%M:%S")
            start_date = timezone.make_aware(start_datetime)
            end_date = timezone.make_aware(end_datetime)
        return start_date, end_date

    def transaction_period_filter(self):
        date_from, date_to = self.get_timeframe()
        transactions = (self.transactions
                        .filter(created_date__range=(date_from, date_to))
                        .order_by("-created_date"))
        return transactions

    def transaction_chart_expenses(self):
        date_from, date_to = self.get_timeframe()
        expenses = (self.transactions
                    .filter(type__name_eng="expense",
                            created_date__range=(date_from, date_to)
                            )
                    .select_related('subcategory__category')
                    .values('subcategory__category_id', 'subcategory__category__name')
                    .annotate(total_amount=Sum('amount'))
                    .order_by('subcategory__category_id'))
        subcategory_names = []
        total_amounts = []

        for item in expenses:
            subcategory_names.append(item['subcategory__category__name'])
            total_amounts.append(item['total_amount'])
        return subcategory_names, total_amounts

from django.db.models import Q
from django.shortcuts import get_object_or_404
from rest_framework import decorators as rest_decorators, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.wallet.models import Account, Transaction
from apps.wallet.serializers import AccountGetSerializer, \
    AccountCreateSerializer, GetTransactionSerializer, CreateTransactionSerializer, AccountUpdateSerializer, \
    TransactionUpdateSerializer
from apps.wallet.service import AccountingEntry, TransactionsFilter, UpdateAccountingEntry


@rest_decorators.api_view(["GET"])
@rest_decorators.permission_classes([IsAuthenticated])
def get_user_accounts_view(request):
    user_id = request.user.id
    accounts = Account.objects.filter(wallet_user=user_id).order_by('name')
    serializer = AccountGetSerializer(accounts, many=True)
    return Response(serializer.data)

@rest_decorators.api_view(["POST"])
@rest_decorators.permission_classes([IsAuthenticated])
def create_account_view(request):
    serializer = AccountCreateSerializer(data=request.data, context={'user_id': request.user.id})
    if serializer.is_valid():
        account = serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@rest_decorators.api_view(["DELETE"])
@rest_decorators.permission_classes([IsAuthenticated])
def delete_account_view(request, id):
    account = get_object_or_404(Account, id=id)
    deleteTransactions = request.data['deleteTransactions']
    if not deleteTransactions:
        UpdateAccountingEntry.update_transaction_account(request.user.id, id)
    account.delete()
    return Response({"message": "Account deleted successfully."}, status=status.HTTP_200_OK)


@rest_decorators.api_view(["GET"])
@rest_decorators.permission_classes([IsAuthenticated])
def get_transactions_view(request):
    user_id = request.user.id
    period = request.GET.get("period")
    start_date = request.GET.get("start_date")
    end_date = request.GET.get("end_date")
    transactions = Transaction.objects.filter(wallet_user=user_id).order_by('-created_date')
    transactionFilter = TransactionsFilter(transactions, period, start_date, end_date)
    transactions = transactionFilter.transaction_period_filter()
    chart_labels, chart_values = transactionFilter.transaction_chart_expenses()
    serializer = GetTransactionSerializer(transactions, many=True)
    response_data = {
        'transactions': serializer.data,
        'chart_labels': chart_labels,
        'chart_values': chart_values}
    return Response(response_data)


@rest_decorators.api_view(["POST"])
@rest_decorators.permission_classes([IsAuthenticated])
def create_transaction_view(request):
    serializer = CreateTransactionSerializer(data=request.data, context={'user_id': request.user.id})
    if serializer.is_valid():
        transaction = serializer.save()
        AccountingEntry(transaction=transaction).create_entry()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@rest_decorators.api_view(["PATCH"])
@rest_decorators.permission_classes([IsAuthenticated])
def update_account_view(request, id):
    try:
        account = Account.objects.get(id=id)
    except Account.DoesNotExist:
        return Response({"detail": "Account does not exist or does not belong to the user."},
                        status=status.HTTP_404_NOT_FOUND)

    serializer = AccountUpdateSerializer(account, data=request.data, partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@rest_decorators.api_view(["PATCH"])
@rest_decorators.permission_classes([IsAuthenticated])
def update_transaction_view(request, id):
    try:
        transaction = Transaction.objects.get(id=id)
    except Transaction.DoesNotExist:
        return Response({"detail": "Transaction does not exist or does not belong to the user."},
                        status=status.HTTP_404_NOT_FOUND)
    serializer = TransactionUpdateSerializer(transaction, data=request.data, partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@rest_decorators.api_view(["DELETE"])
@rest_decorators.permission_classes([IsAuthenticated])
def delete_transaction_view(request, id):
    transaction = get_object_or_404(Transaction, id=id)
    AccountingEntry(transaction=transaction).delete_entry_accounts_update()
    transaction.delete()
    return Response({"message": "Transaction deleted successfully."}, status=status.HTTP_200_OK)




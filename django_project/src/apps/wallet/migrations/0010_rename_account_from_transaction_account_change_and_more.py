# Generated by Django 5.0.6 on 2024-07-01 05:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0009_alter_transaction_subcategory'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transaction',
            old_name='account_from',
            new_name='account_change',
        ),
        migrations.RenameField(
            model_name='transaction',
            old_name='account_to',
            new_name='account_transfer',
        ),
    ]

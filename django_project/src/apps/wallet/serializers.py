from datetime import datetime, timedelta

from django.db import transaction
from django.utils import timezone
from rest_framework import serializers

from apps.authentication.models import WalletUser
from apps.category.models import SubCategory
from apps.category.serializers import SubcategorySerializer
from apps.currency.models import Currency
from apps.currency.serializers import CurrencySerializer
from apps.wallet.models import Account, Transaction, TransactionType
from apps.wallet.service import UpdateAccountingEntry


class AccountCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'name', 'balance']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        return representation

    def create(self, validated_data):
        currency = Currency.objects.filter(char_code="RUB").first()
        user_id = self.context['user_id']
        user = WalletUser.objects.get(id=user_id)
        account = Account.objects.create(
            name=self.validated_data['name'],
            balance=self.validated_data['balance'],
            currency=currency,
            wallet_user=user)
        return account


class AccountGetSerializer(serializers.ModelSerializer):
    currency = CurrencySerializer()
    class Meta:
        model = Account
        fields = ['id', 'name', 'balance', 'currency', 'visible']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        return representation


class AccountUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['name', 'balance']


class TransactionTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionType
        fields = "__all__"


class AccountNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['id', 'name']

class GetTransactionSerializer(serializers.ModelSerializer):
    type = TransactionTypeSerializer()
    subcategory = SubcategorySerializer()
    account_change = AccountNameSerializer()
    account_transfer = AccountNameSerializer()
    currency = CurrencySerializer()
    category = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = ['id', 'amount', 'created_date', 'type', 'category', 'subcategory', 'account_change', 'account_transfer', 'comment', 'currency']

    def get_category(self, obj):
        if obj.subcategory and obj.subcategory.category:
            return {
                'id': obj.subcategory.category.id,
                'name': obj.subcategory.category.name
            }
        return None

class CreateTransactionSerializer(serializers.Serializer):
    comment = serializers.CharField()
    amount = serializers.DecimalField(max_digits=20, decimal_places=2)
    type_str = serializers.CharField(write_only=True)
    subcategory_id = serializers.IntegerField(allow_null=True)
    account_change_id = serializers.IntegerField()
    account_transfer_id = serializers.IntegerField(allow_null=True)
    class Meta:
        fields = ['comment', 'amount', 'type_str', 'subcategory_id', 'account_change_id', 'account_transfer_id']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation.pop('type_str', None)
        return representation

    def create(self, validated_data):
        currency = Currency.objects.filter(char_code="RUB").first()

        user_id = self.context['user_id']
        user = WalletUser.objects.get(id=user_id)

        tr_type = TransactionType.objects.filter(name_eng=self.validated_data['type_str']).first()

        subcategory_id = self.validated_data['subcategory_id']
        subcategory = None
        if subcategory_id is not None:
            subcategory = SubCategory.objects.get(id=subcategory_id)

        account_change = Account.objects.get(id=self.validated_data['account_change_id'])

        account_transfer_id = self.validated_data['account_transfer_id']
        account_transfer = None
        if account_transfer_id is not None:
            account_transfer = Account.objects.get(id=account_transfer_id)

        transaction = Transaction.objects.create(
            comment=self.validated_data['comment'],
            amount=self.validated_data['amount'],
            type=tr_type,
            wallet_user=user,
            currency=currency,
            subcategory=subcategory,
            account_change=account_change,
            account_transfer=account_transfer
        )
        return transaction

class TransactionUpdateSerializer(CreateTransactionSerializer):
    new_date = serializers.CharField(required=False)
    new_time = serializers.CharField(required=False)
    class Meta:
        model = Transaction
        fields = ['comment', 'amount', 'type_str', 'subcategory_id', 'account_change_id', 'account_transfer_id',
                  'new_date', 'new_time']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation.pop('type_str', None)
        representation.pop('new_date', None)
        representation.pop('new_time', None)
        return representation
    def update(self, instance, validated_data):
        '''
            Для того, чтобы корректно сделать смещение для пользователя необходимо хранить его timezone
            Не будем усложнять и захардкодим смещение по времени как для tz="Novosibirsk"
            Чтобы при отладке все работало корректно
        '''
        new_date = validated_data.pop('new_date', None)
        new_time = validated_data.pop('new_time', None)
        with transaction.atomic():
            instance.comment = validated_data.get('comment', instance.comment)
            UpdateAccountingEntry(instance, validated_data['type_str'],
                                  validated_data['subcategory_id'], validated_data['account_change_id'],
                                  validated_data['account_transfer_id'], validated_data['amount']).update_entry()

            if new_date and new_time:
                combined_datetime_str = f"{new_date} {new_time}"
                combined_datetime = timezone.make_aware(datetime.strptime(combined_datetime_str, '%Y-%m-%d %H:%M'))
                instance.created_date = combined_datetime - timedelta(hours=7)
            instance.save()
        return instance
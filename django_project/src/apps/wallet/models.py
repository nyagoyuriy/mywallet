from django.db import models

from apps.authentication.models import WalletUser
from apps.category.models import SubCategory, Category
from apps.currency.models import Currency

class Account(models.Model):
    name = models.CharField()
    balance = models.DecimalField(max_digits=23, decimal_places=2)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)
    wallet_user = models.ForeignKey(WalletUser, on_delete=models.CASCADE)
    visible = models.BooleanField(default=True)


class TransactionType(models.Model):
    name = models.CharField(max_length=100)
    name_eng = models.CharField(max_length=100)


class Transaction(models.Model):
    type = models.ForeignKey(TransactionType, on_delete=models.CASCADE)
    subcategory = models.ForeignKey(SubCategory, on_delete=models.CASCADE, null=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    comment = models.TextField()
    account_change = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='transactions_from')
    account_transfer = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='transactions_to', null=True)
    wallet_user = models.ForeignKey(WalletUser, on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE)

